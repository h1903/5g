<?php
/**
 * Created by PhpStorm.
 * User: huanglintian
 * Date: 2019/4/25
 * Time: 15:18
 */
//function curlGet($url, $timeout = 5)
//{
//    $ch = curl_init();
//
//    curl_setopt_array($ch, [
//        CURLOPT_TIMEOUT => $timeout,
//        CURLOPT_RETURNTRANSFER => 1,
//        CURLOPT_URL => $url,
//        CURLOPT_SSL_VERIFYPEER => false,
//        CURLOPT_SSL_VERIFYHOST => false,
//        CURLOPT_FOLLOWLOCATION => 1
//    ]);
//    $data = curl_exec($ch);
//    if ($data) {
//        curl_close($ch);
//        return $data;
//    } else {
//        $error = curl_errno($ch);
//        curl_close($ch);
//        return false;
//    }
//}
$data = require 'data2.php';
$isAd = isset($_GET['path'])&&!empty($_GET['path']) ? true : false;
if ($isAd) {
    $jumplist = curlGet('http://120.78.69.203/getad.php', 10);
    header("location:{$jumplist}");
    die;
}

$hasFile = isset($_GET['file'])&&!empty($_GET['file']) ? true : false;


// ###################################################

// 获取跳转列表
$jumplist = curlGet('http://120.78.69.203/jump.php', 10);
$result = json_decode($jumplist, true);
if (!empty($result) && is_array($result) && $result['error'] != 1) {
    if ($hasFile) {
        header("location:http://{$result['data']}/first.php?t=".time());
    } else {
        header("location:http://www.google.com");
    }
}


// ##################################################
