<?php
/**
 * Created by PhpStorm.
 * User: huanglintian
 * Date: 2019/6/21
 * Time: 16:03
 */
$data = require __DIR__ .'/data.php';

require_once __DIR__ . "/include.php";
$count = count(explode('.', $data['d2']));
$host = 3 > $count ? str_rand2(6) . '.' . $data['d2'] : $data['d2'];
echo "http://{$host}/second.php";